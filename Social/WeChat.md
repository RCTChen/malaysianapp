# WeChat

#### 流程

1. ** 登入時使用WeChat登入 ** [[1.0 登入畫面]](https://rctchen.gitlab.io/malaysianapp/Wireframe/1.0.html)
2. ** 大廳畫面中多出[WeChat分享]功能 ** [[2.0 大廳]](https://rctchen.gitlab.io/malaysianapp/Wireframe/2.0.html)
3. ** 將產品轉化成圖片、文字等資訊，讓使用者可以直接分享給WeChat好友、群聊，透過活動等方式引導使用者發佈分享在WeChat朋友圈中(圖片、文字資訊中嵌入產品鏈接、二維碼等資訊)。 **

> 將產品資訊分享到目標族群中的好友、微信群內，並鼓勵及引導轉發分享朋友圈。

![WeChat](./WeChat.jpg)

> 以程序卡片的形式分享給好友，點擊開啟APP。

![WeChat_2](./WeChat_2.jpg)
# 社群功能

#### MalaysianAPP 產品中主要著重的社群功能
1. ** [FB](https://rctchen.gitlab.io/malaysianapp/Social/FB.html) **
2. ** [WeChat](https://rctchen.gitlab.io/malaysianapp/Social/WeChat.html) **
3. ** [紅包](https://rctchen.gitlab.io/malaysianapp/Social/Money.html) **

藉由與當地市場上熱門且規模較大的社群平台功能API串接，讓使用者得以運用該渠道登入產品，並運用社群平台上快速方便的分享功能，達到產品曝光推廣的目的。
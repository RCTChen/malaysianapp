# 紅包功能

#### 流程

1. ** 登入後，可看見紅包系統圖示 ** [[2.0 大廳]](https://rctchen.gitlab.io/malaysianapp/Wireframe/2.0.html)
2. ** 當達成特定條件，紅包系統圖示提示玩家點擊。 **
3. ** 點開紅包系統，可在視窗內領取定額的額外彩金。 ** [[2.0a 紅包]](https://rctchen.gitlab.io/malaysianapp/Wireframe/2.0a.html)

> 轉化JP彩金，以紅包方式派送，透過結合相關活動，提升產品對於使用者的誘因，增加黏著度。

* ** 1%返點為例，使用者每進行100有效投注額度，累積1點紅包，集滿100點可領取。 **
* ** 結合每日任務，每日登入產品並進行遊戲，達到指定條件(有效投注額度門檻 等)即可領取定額紅包。 **
* ** 推廣特定遊戲，達到該遊戲指定條件(有效投注額度門檻 等)即可領取定額紅包。**

#### 參考圖可視[[2.0a 紅包]](https://rctchen.gitlab.io/malaysianapp/Wireframe/2.0a.html)
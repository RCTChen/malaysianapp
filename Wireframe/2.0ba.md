# 2.0ba 密碼修改

> 由[[2.0b 菜單]](https://rctchen.gitlab.io/malaysianapp/Wireframe/2.0b.html)點選[2.0ba 密碼修改] 將彈出視窗可修改密碼(不跳轉頁面)

* 資料輸入 提示效果可如[[1.1 建議回報]](https://rctchen.gitlab.io/malaysianapp/Wireframe/1.1.html)參考圖，在框格內底字提示

![2.0ba 密碼修改](./2.0ba.jpg)

#### 參考圖(PUSSY888_修改密碼頁面)

![參考圖](./2.0ba_2.jpg)
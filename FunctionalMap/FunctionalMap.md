# Functional Map

> MalaysianAPP產品中所包含之設定、使用以及相關資訊顯示之功能。

## 重點分類

#### 登入方式及延伸功能

* ** FB登入 ** → ** FB分享 **
* ** 微信登入 ** → ** 微信分享 **

#### 特殊獎勵機制

* ** [紅包功能](https://rctchen.gitlab.io/malaysianapp/Wireframe/2.0a.html) **

#### 遊戲分類及表現方式

* ** 大廳中央圖卡輪播 **

![Functional Map](./Functional Map_20190520.jpg)
# FB

#### 流程

1. ** 登入時使用FB登入 ** [[1.0 登入畫面]](https://rctchen.gitlab.io/malaysianapp/Wireframe/1.0.html)
2. ** 大廳畫面中多出[FB分享]功能 ** [[2.0 大廳]](https://rctchen.gitlab.io/malaysianapp/Wireframe/2.0.html)
3. ** 可直接在使用者FB快速發布內容且自動串接產品資訊及鏈接 **

![FB](./FB.jpg)

也可在產品內直接添加FB粉絲團鏈接，透過活動及獎勵直接將使用者導向FB紛絲團，進一步在粉絲團上進行行銷操作。

> 加入粉絲團送彩金、評價5星...
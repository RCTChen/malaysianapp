# 2.0c 最近遊玩

> 由[[2.0 大廳]](https://rctchen.gitlab.io/malaysianapp/Wireframe/2.0.html)選擇[2.0c 最近遊玩] 後將會由右至左彈出選單(不跳轉頁面)

* 右方區塊可擺放 推薦遊戲、熱門遊戲 等圖卡(上下拖曳滑動)
* 點選任一空白地方則頁面關閉，回到[[2.0 大廳]](https://rctchen.gitlab.io/malaysianapp/Wireframe/2.0.html)
* 此頁面為 ** 半透明 ** 效果，如參考圖

![2.0c 最近遊玩](./2.0c.jpg)

#### 參考圖(PUSSY888_最近遊玩視窗)

![參考圖](./2.0c_2.jpg)
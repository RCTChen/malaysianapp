# MalaysianAPP

## 關於此份文件

> 列出Functional Map、UI Flow、基本Wireframe以供討論，針對MalaysianAPP產品進行優化及調整。

### [Functional Map](https://rctchen.gitlab.io/malaysianapp/FunctionalMap/FunctionalMap.html)

MalaysianAPP產品中所包含之設定、使用以及相關資訊顯示之功能。

### [UI Flow](https://rctchen.gitlab.io/malaysianapp/UIFlow/UIFlow.html)

Functional Map之功能在APP頁面間的層級關係。

### [基本Wireframe](https://rctchen.gitlab.io/malaysianapp/Wireframe/Wireframe.html)

以基本線框圖標示APP頁面中各UI元件的大致位置及元件效果。
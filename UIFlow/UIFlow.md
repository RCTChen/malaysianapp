# UI Flow

> Functional Map之功能在APP頁面間的層級關係。

## 重點分類

* ** [1.0 登入畫面](https://rctchen.gitlab.io/malaysianapp/Wireframe/1.0.html) **
   * ** [1.0a 註冊登入](https://rctchen.gitlab.io/malaysianapp/Wireframe/1.0a.html) **
* ** [2.0 大廳畫面](https://rctchen.gitlab.io/malaysianapp/Wireframe/2.0.html) **
   * ** [2.0a 紅包](https://rctchen.gitlab.io/malaysianapp/Wireframe/2.0a.html) **
   * ** [2.0b 菜單](https://rctchen.gitlab.io/malaysianapp/Wireframe/2.0b.html) **
   * ** 中間輪播 **
* ** FB登入、微信登入、Gmail登入 另作討論 **

![UI Flow](./UI Flow_20190520.jpg)